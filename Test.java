import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            integers.add(i);
        }
        test(integers);
    }

    //Делятся на 3, не делятся на 5, сумма цифр меньше 10
    //999 -> false
    public static void test(List<Integer> numbers) {
        numbers.stream()
                .filter(number -> number % 3 == 0 && number % 5 != 0 && get(number))
                .forEach(System.out::println);
    }

    public static boolean get(Integer number) {

        int result = 0;
        while (number > 0) {
            result = result + number % 10;
            number = number /10;
        }

        return result < 10;
    }
}
