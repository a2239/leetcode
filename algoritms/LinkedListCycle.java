package algoritms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinkedListCycle {


//  Definition for singly-linked list.
  class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
          val = x;
          next = null;
      }
  }

    public boolean hasCycle(
//            ListNode head
    ) {
        ListNode head = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        head.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode2;

        ListNode slowPointer = head, fastPointer = head;
        while (fastPointer !=null && fastPointer.next != null){
            slowPointer = slowPointer.next;
            fastPointer = fastPointer.next.next;
            System.out.println("slow pointer - "+ slowPointer.val + " fast pointer - " + fastPointer.val);
            if (slowPointer == fastPointer){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        LinkedListCycle linkedListCycle = new LinkedListCycle();
        boolean b = linkedListCycle.hasCycle();
        System.out.println(b);
    }

}
