package algoritms;

import java.util.Arrays;
import java.util.Objects;

//Write a function to find the longest common prefix string amongst an array of strings.
//
//If there is no common prefix, return an empty string "".
public class LongestCommonPrefix {


    public String longestCommonPrefix(String[] strs) {

        if (strs == null || strs.length == 0) {
            return "";
        }

        Arrays.sort(strs);

        String first = strs[0];
        String last = strs[strs.length - 1];

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < Math.min(first.length(), last.length()); i++) {

            if (Objects.equals(first.charAt(i), last.charAt(i))) {
                builder.append(first.charAt(i));
            } else {
                return builder.toString();
            }
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        System.out.println(new LongestCommonPrefix().longestCommonPrefix(new String[]{"flower", "flow", "flight"}));
        System.out.println(new LongestCommonPrefix().longestCommonPrefix(new String[]{"dog", "racecar", "car"}));
        System.out.println(new LongestCommonPrefix().longestCommonPrefix(new String[]{""}));
    }

}
