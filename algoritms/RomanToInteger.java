package algoritms;

import java.util.HashMap;
import java.util.Map;

//Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
//For example, 2 is written as II in Roman numeral, just two ones added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
//Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:
//I can be placed before V (5) and X (10) to make 4 and 9.
//X can be placed before L (50) and C (100) to make 40 and 90.
//C can be placed before D (500) and M (1000) to make 400 and 900.
//Given a roman numeral, convert it to an integer.
public class RomanToInteger {

    public int romanToInt(String s) {

        Map<Character, Integer> intToRoman = Map.of(
                'I', 1,
                'V', 5,
                'X', 10,
                'L', 50,
                'C', 100,
                'D', 500,
                'M', 1000
        );


        int result = 0;
        for (int i = s.length()-1; i >= 0; i--) {
            char roman = s.charAt(i);
            Integer numb = intToRoman.get(roman);
            //LVIII = 58
                          //1900                    1990                    1994
            //MCMXCIV M-1000 (C-100 M-1000)[900] (X - 10, C - 100)[90] (I - 1, V - 5)[4]

            // 0 < 1000 = 1000+0 = 1000
            //1000 < 100 * 400 = 1000 + 100 =1100
            //1100 < 1000 * 4 = 1100 - 1000 = 100
            //100 < 1000 * 4 = 1000 - 100 = 900

            //V 5  0 < 5 = 0 + 5
            //I 1 5 < 4 = 5 - 1 = 4
            //C 100 4 < 100 * 4  = 4 + 100 = 104
            //X 10 104 < 10 * 4 = 104 - 10 = 94
            //M 1000 94 < 4000  = 94 + 1000 = 1094
            //C 100 1094 < 400 = 1094 - 100 = 994
            //M 1000 994 < 4000 = 994 + 1000 = 1994

            int temp = numb * 4;
            if (result < temp) {
                result = result + numb;
            } else {
                result = result - numb;
            }
        }



        return result;
        // I before 5(V) and I before 10(X) = 4 or 9
        // X before 50(L) and X before 100(C) = 40 or 90
        // C before 500(D) and C before 1000(M) = 400 or 900

        //  (1000) (900)  (90)  (4)
        //     M     CM     XC   IV
    }

    public static void main(String[] args) {
        //3
        System.out.println(new RomanToInteger().romanToInt("III"));
        //58
        System.out.println(new RomanToInteger().romanToInt("LVIII"));
        //1994
        System.out.println(new RomanToInteger().romanToInt("MCMXCIV"));
    }
}
