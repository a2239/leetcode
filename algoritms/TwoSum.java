package algoritms;

import java.util.*;

//Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
//You may assume that each input would have exactly one solution, and you may not use the same element twice.
//You can return the answer in any order.
public class TwoSum {

    public int[] twoSum(int[] nums, int target) {

        Map<Integer, Integer> indexByFirstPair = new HashMap<>();

        List<Integer> res = new ArrayList<>();
        //3, 2, 4 : 6
        for (int i = 0; i < nums.length; i++) {

            int needSecond = target - nums[i];

            if (indexByFirstPair.containsKey(needSecond)) {
                res.add(indexByFirstPair.get(needSecond));
                res.add(i);
            }

            indexByFirstPair.put(nums[i], i);
        }

        return res.stream()
                .mapToInt(result -> result)
                .toArray();
    }


    public static void main(String[] args) {
        Arrays.stream(new TwoSum().twoSum(new int[]{3, 2, 4}, 6)).forEach(System.out::println);
        Arrays.stream(new TwoSum().twoSum(new int[]{3, 3}, 6)).forEach(System.out::println);
        Arrays.stream(new TwoSum().twoSum(new int[]{2, 7, 11, 15}, 9)).forEach(System.out::println);
    }
}

