package algoritms

class TwoSumKt {
    fun twoSum(nums: IntArray, target: Int): IntArray {
        val numByTarget = mutableMapOf<Int, Int>()
        //[3,2,4]
        val result = mutableListOf<Int>()

        nums.forEachIndexed { index, firstPair ->
            // 6 - 3 = 3    (3/3)
            // 6 - 2 = 4    (2/4)
            // 6 - 4 = 2    (4/2)
            val needSeconPair = target - firstPair

            if (numByTarget.containsKey(needSeconPair)) {
              numByTarget[needSeconPair]?.also {
                  result.add(it)
                  result.add(index)
              }
            }

            numByTarget[firstPair] = index
        }

        return result.toIntArray()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {

            TwoSumKt().twoSum(intArrayOf(3, 2, 4), 6).forEach {
                println(it)
            }

            TwoSumKt().twoSum(intArrayOf(3, 3), 6).forEach {
                println(it)
            }

            TwoSumKt().twoSum(intArrayOf(2,7,11,15), 9).forEach {
                println(it)
            }

        }
    }
}