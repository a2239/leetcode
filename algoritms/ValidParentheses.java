package algoritms;//Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
//
//An input string is valid if:
//
//Open brackets must be closed by the same type of brackets.
//Open brackets must be closed in the correct order.
//Every close bracket has a corresponding open bracket of the same type.

import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class ValidParentheses {

    public static boolean isValid(String s) {

        if (s.length() % 2 != 0) {
            return false;
        }

        Deque<Character> characters = new LinkedList<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == ')' && Objects.equals('(', characters.peekLast())) {
                characters.pollLast();
            } else if (c == ']' && Objects.equals('[', characters.peekLast())) {
                characters.pollLast();
            } else if (c == '}' && Objects.equals('{', characters.peekLast())) {
                characters.pollLast();
            } else {
                characters.add(c);
            }
        }

        return characters.isEmpty();
    }


    public static void main(String[] args) {
        String parentheses = "(){}[]";
        String invalid = "(]";
        String invalid1 = "()]]]";
        String valid = "{[()]}";
        String invalid2 = "){";
        String invalid3 = "([}}])";
        String invalid4 = "([)]";
        System.out.println(isValid(parentheses));
        System.out.println(isValid(invalid));
        System.out.println(isValid(invalid1));
        System.out.println(isValid(valid));
        System.out.println(isValid(invalid2));
        System.out.println(isValid(invalid3));
        System.out.println(isValid(invalid4));
    }
}
