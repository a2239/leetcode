package multithread;

public class DeadLock {

    public static void main(String[] args) throws InterruptedException {
        Account account1 = new Account();
        Account account2 = new Account();

        TransferThread thread1 = new TransferThread(account1, account2);
        TransferThread thread2 = new TransferThread(account2, account1);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();
    }


    public static class Account {
        private Integer balance = 10000;

        public void add(int amount) {
            balance = balance + amount;
        }

        public boolean takeOff(int amount) {
            if (this.balance >= amount) {
                balance = balance - amount;
                return true;
            }
            return false;
        }
    }

    public static class TransferThread extends Thread {

        private final Account from;
        private final Account to;

        public TransferThread(Account from, Account to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public void run() {
            System.out.println("Thread " + Thread.currentThread().getName() + " started");
            for (int i = 0; i <100000; i++) {
                System.out.println("Thread " + Thread.currentThread().getName() + " counter -" + i);
                synchronized (from) {
                    synchronized (to) {
                        if (from.takeOff(10)) {
                            to.add(10);
                        }
                    }
                }
            }
            System.out.println("Thread " + Thread.currentThread().getName() + " finished");
        }
    }
}
